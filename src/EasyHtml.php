<?php

namespace snackpatruljen;

class EasyHtml
{
    public static function render_children(string $accumulator = "", array $children = array())
    {
        foreach ($children as $child) {
            if (is_array($child)) {
                EasyHtml::render_children($accumulator, $child);
            } else {
                $accumulator .= $child;
            }
        }

        return $accumulator;
    }

    static function tag(string $tag, array $attributes, array $children = [])
    {
        $attributes = array_filter($attributes);
        $mapped_attrs = array_map(function ($k, $v) {
            return $k . "=" . $v; // Convert to html attributes
        }, array_keys($attributes), array_values($attributes));
        $reduced_attrs = array_reduce($mapped_attrs, function ($carry, $item) {
            return $carry . " " . $item; // Concat into a single string
        });
        $rendered_children = EasyHtml::render_children("", $children);
        return "<{$tag} {$reduced_attrs}>{$rendered_children}</{$tag}>";
    }

    static function div(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("div", $attrs, $children);
    }

    static function li(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("li", $attrs, $children);
    }

    static function button(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("button", $attrs, $children);
    }

    static function a(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("a", $attrs, $children);
    }

    static function img(array $attrs = [])
    {
        return EasyHtml::tag("img", $attrs);
    }

    static function ul(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("ul", $attrs, $children);
    }

    static function span(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("span", $attrs, $children);
    }

    static function h1(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("h1", $attrs, $children);
    }

    static function h2(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("h2", $attrs, $children);
    }

    static function h3(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("h3", $attrs, $children);
    }

    static function h4(array $attrs = [], array $children = [])
    {
        return EasyHtml::tag("h4", $attrs, $children);
    }
}

// $global_attrs = [
//     "accesskey",
//     "autocapitalize",
//     "class",
//     "contenteditable",
//     "contextmenu",
//     "data-*",
//     "dir",
//     "draggable",
//     "dropzone",
//     "hidden",
//     "id",
//     "inputmode",
//     "is",
//     "itemid",
//     "itemprop",
//     "itemref",
//     "itemscope",
//     "itemtype",
//     "lang",
//     "part",
//     "slot",
//     "spellcheck",
//     "style",
//     "tabindex",
//     "title",
//     "translate",
// ];
